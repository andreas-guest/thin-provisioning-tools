Source: thin-provisioning-tools
Section: admin
Priority: optional
Maintainer: Debian LVM Team <team+lvm@tracker.debian.org>
Uploaders: Bastian Blank <waldi@debian.org>
Build-Depends:
 debhelper (>= 9),
 dh-autoreconf,
 gawk,
 libaio-dev,
 libboost-dev,
 libexpat1-dev,
 libgtest-dev,
 google-mock,
Standards-Version: 3.9.5
Vcs-Git: https://gitlab.com/debian-lvm/thin-provisioning-tools.git
Vcs-Browser: https://gitlab.com/debian-lvm/thin-provisioning-tools

Package: thin-provisioning-tools
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Description: Tools for handling thinly provisioned device-mapper meta-data
 This package contains tools to handle meta-data from the device-mapper
 thin target.  This target allows the use of a single backing store for multiple
 thinly provisioned volumes.  Numerous snapshots can be taken from such
 volumes.  The tools can check the meta-data for consistency, repair damaged
 information and dump or restore the meta-data in textual form.
